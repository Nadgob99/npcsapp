export interface Npc{
    id: number | string,
    name: string,
    hitPoints: number,
    strenght: number,
    defense: number,
    intelligence: number
    class: NpcClassType
}

enum NpcClassType {
    mage = 1,
    hobbit = 2,
    archer = 3,
    knight = 4,
}