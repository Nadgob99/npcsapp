# README #
NpcsApp

### General Info ###

#### Description ####
This project is a simple hybrid application that manages core details about a game's npcs.

#### Features ####
* Authentication
* Simple CRUD operations on a list of Npc entities 


### Technologies ###
Project is created with:

* React: 16.13.0
* Ionic React: 5.07
* Typescript: 3.8.3

### Setup ###
Open a new terminal and execute next commands:

* npm install
* ionic serve 

### Who do I talk to? ###

* Repository owner: nadgob99@gmail.com